let crypto;
try {
  crypto = require('crypto');
} catch (err) {
  throw new Error('crypto support is disabled!');
}
const fs = require('fs');

function fromReadStream(fileReadStream) {
  return new Promise((resolve) => {
    const hash = crypto.createHash('sha256');
    fileReadStream.on('readable', () => {
      // Only one element is going to be produced by the
      // hash stream.
      const data = fileReadStream.read();
      if (data)
        hash.update(data);
      else {
        resolve(hash.digest('hex'));
      }
    });
  });
}

function fromFileName(fileName) {
  const input = fs.createReadStream(fileName);
  return fromReadStream(input);
}
  
module.exports = {
  fromReadStream,
  fromFileName,
}
