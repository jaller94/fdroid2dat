'use strict';
const fs = require('fs');
const fsPromises = require('fs').promises;
const https = require('https');
const path = require('path');

const rootFolder = process.env.MODE === 'PRODUCTION' ? 'prod' : 'staging';

const {fromFileName} = require('./sha256.js');

const Dat = require('dat-node');
const xml2js = require('xml2js');

function downloadFile(url, fileWriteStream) {
  return new Promise((resolve, reject) => {
    https.get(url, (res) => {
      if (res.statusCode !== 200) {
        console.log('statusCode:', res.statusCode);
        console.log('headers:', res.headers);
        console.log('body:', res.body);
      }
      
      res.on('data', (data) => {
        fileWriteStream.write(data);
        process.stdout.write('.');
      });
      res.on('end', () => {
        fileWriteStream.end();
        resolve();
      });
    }).on('error', (e) => {
      reject(e);
    });
  });
}

async function assertFileExists(filePath) {
  const stat = await fsPromises.lstat(filePath);
  if (!stat.isFile()) throw new Error('Is not a file!');
  return;
}

async function assertFileSHA256(filePath, expected) {
  const actual = await fromFileName(filePath)
  if (actual !== expected) {
    throw new Error(`SHA256 did not match: ${actual} (expected: ${expected})`);
  }
  return;
}

function getDatLink(folderPath) {
  return new Promise((resolve, reject) => {
    Dat(folderPath, {secretDir: path.join(rootFolder, 'secret_keys')}, function (err, dat) {
      if (err) reject(err);
      resolve(dat.key.toString('hex'));
    });
  });
}

function serveDat(folderPath) {
  return new Promise((resolve, reject) => {
    Dat(folderPath, {secretDir: path.join(rootFolder, 'secret_keys')}, function (err, dat) {
      if (err) reject(err);
      dat.importFiles();
      const network = dat.joinNetwork(() => {
        dat.leaveNetwork();
        console.log(`Leaving ${folderPath}`);
      });
      /*
      network.on('connection', (connection, info) => {
        console.log(info.host, info.initiator);
      })
      */
      //console.log(`serving ${dat.key.toString('hex')}`);
      resolve();
    });
  });
}

async function clearOldApks(folderPath, latest) {
  const files = await fsPromises.readdir(folderPath);
  for (const file of files) {
    if (file.endsWith('.apk') && file !== latest) {
      await fsPromises.unlink(path.join(folderPath, file));
      console.log(`Deleting outdated APK: ${file}`);
    }
  }
}

async function createPackage(application) {
  //console.dir(application);
  //console.dir(application.package[0]);
  const folder = path.join(rootFolder, 'dats', application['$'].id);
  await fsPromises.mkdir(folder, {recursive: true});
  clearOldApks(folder, application.package[0].apkname[0]);
  const apkPath = `${folder}/${application.package[0].apkname[0]}`;
  try {
    await assertFileExists(apkPath);
    console.log('APK exists ✔️');
  } catch(err) {
    if (err.code !== 'ENOENT') console.warn(err);
    console.log('Downloading APK…');
    const apkFile = fs.createWriteStream(`${folder}/${application.package[0].apkname}`);
    await downloadFile(`https://mirror.cyberbits.eu/fdroid/repo/${application.package[0].apkname}`, apkFile);
    await assertFileSHA256(apkPath, application.package[0].hash[0]['_']);
    console.log('APK hash matched ✔️');
  }
  const readme = `# ${application.name[0]}\n\n${application.desc[0]}`;
  await fsPromises.writeFile(`${folder}/readme.md`, readme);
  const metadata = {
    convention: '1',
    apk: `/${application.package[0].apkname[0]}`,
    readme: '/readme.md',
  };
  await fsPromises.writeFile(`${folder}/metadata.json`, JSON.stringify(metadata, null, 2));
  const datData = {
    title: application.name[0],
    description: application.summary[0],
    type: [],
    url: `dat://${await getDatLink(folder)}`,
  };
  await fsPromises.writeFile(`${folder}/dat.json`, JSON.stringify(datData, null, 2));
  return {
    title: application.name[0],
    path: folder,
    key: await getDatLink(folder),
  }
}

async function generateIndex(applications, archives) {
  const folder = path.join(rootFolder, 'index');
  await fsPromises.mkdir(folder, {recursive: true});
  const datData = {
    title: 'F-Droid mirror by Jaller',
    description: 'A mirror of the F-Droid Android repository served via DAT',
    type: ['website'],
    url: `dat://${await getDatLink(folder)}`,
  };
  const inputHtml = await fsPromises.readFile(`site/index.html`, 'utf8');
  await fsPromises.writeFile(`${folder}/dat.json`, JSON.stringify(datData, null, 2));
  let html = '<ul>';
  for (const archive of archives) {
    html += `<li><a href="dat://${archive.key}">${archive.title}</a></li>\n`;
  }
  html += '</ul>';
  const outputHtml = inputHtml.replace('{{ applications }}', html);
  await fsPromises.writeFile(`${folder}/index.html`, outputHtml);
  
  const csvData = archives.map(a => a.key).join('\n') + '\n';
  await fsPromises.writeFile(`${folder}/index.csv`, csvData);
  
  await serveDat(folder);
}

async function createPackages(applications) {
  applications = await filterPackages(applications);
  const archives = [];
  const failed = [];
  let i = 0;
  for (const application of applications) {
    const {id} = application['$'];
    console.log(`Starting ${id}`);
    try {
      archives.push(await createPackage(application));
    } catch(err) {
      failed.push(id);
    }
    i++;
    console.log(`Processed ${i} of ${applications.length}`);
  }
  if (failed.length !== 0) {
    console.error(failed);
    //throw new Error('packages failed to build');
  }
  for (const archive of archives) {
    await serveDat(archive.path);
  }
  await generateIndex(applications, archives);
}

async function filterPackages(applications) {
  let whitelist = (await fsPromises.readFile('lists/all.txt', 'utf8')).split('\n');
  whitelist.push(...(await fsPromises.readFile('lists/recommended.txt', 'utf8')).split('\n'));
  whitelist.push(...applications.slice(-100).map(app => app['$'].id));
  return applications.filter((application) => whitelist.includes(application['$'].id));
}
 
var parser = new xml2js.Parser();
fs.readFile(__dirname + '/index.xml', function(err, data) {
  if (err) throw err;
  parser.parseString(data, function (err, result) {
    if (err) throw err;
    createPackages(result.fdroid.application).then(() => {
      console.log('Done');
    }).catch(console.error);
  });
});
