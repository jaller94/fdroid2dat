# F-Droid2Dat

This project downloads apps from [F-Droid](https://f-droid.org/) and create/update one dat archives per app.
The dat archives can then be loaded by [Dat Installer](https://github.com/staltz/dat-installer) to install them on an Android phone.

## Run it

```bash
npm install
mkdir -p staging/{dats,index,secret_keys}
bash download.sh  # Retrieve latest list of apps (creates index.xml)
node index.js  # Create / Update apps and host them until they are synced
```
